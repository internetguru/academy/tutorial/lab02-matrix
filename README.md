[![pipeline status](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/badges/master/pipeline.svg)](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/-/pipelines?ref=master)
[![compile](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/builds/artifacts/master/raw/.results/compile.svg?job=evaluate)](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/-/jobs/artifacts/master/file/.results/compile.log?job=evaluate)
[![checkstyle](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/builds/artifacts/master/raw/.results/checkstyle.svg?job=evaluate)](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/-/jobs/artifacts/master/file/.results/checkstyle.log?job=evaluate)
[![test](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/builds/artifacts/master/raw/.results/test.svg?job=evaluate)](https://gitlab.com/internetguru/academy/tutorial/java01-introduction/-/jobs/artifacts/master/file/.results/test.log?job=evaluate)

# Java02: Matrix

> This assignment project represents 2D matrices with two different constructors and several basic operations (methods). Note: The `Matrix` class represents `Matrix` objects, therefore its methods are not static. In order to call these methods, you must call them from an object of the `Matrix` class.
